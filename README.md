# iChecker WebServices

This WebServices has purposes only to supply the iChecker App.


# Requirements

- NodeJS 8.16.0+
- MongoDB

## Configuration

- Rename the file `.env.exmaple` to `.env`
- If you are not using Docker. Setup your MongoDB config

## Make you life easy

This WebServices is dockerized, so if you wish you could just setup the container with `docker-compose up -d`

## Running WITHOUT Docker

- First install the modules with: `npm install`
- After everything is fine, you should run: `npm start`
