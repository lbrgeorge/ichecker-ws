// HTTP Server
const http = require('./http')

// Database
const database = require('./database')

// JWT Wrapper
const jwt = require('./jwt')

// Utils
const logger = require('./utils/logger')
const { Color } = require('./utils/constants')

class App {
  /**
   * Initialize application
   */
  async init() {
    const { HTTP_PORT } = process.env

    try {
      // Initialize database
      await database.init()
  
      // Initialize HTTP server
      await http.configure(HTTP_PORT).init()
  
      // Validate jwt keys
      if (!await jwt.validateKeys()) {
        logger('Generating RSA keys for JWT sign', Color.FgCyan)
        await jwt.generateKeys()
      }

      logger(`iChecker WebServices initialized! Listening on ports: ${HTTP_PORT}`, Color.FgGreen)
    } catch (error) {
      logger('Failed to initialize WebServices!')
      
      if (error.name && error.name === 'MongoNetworkError') {
        logger('Database not ready. Retrying...', Color.FgRed)

        setTimeout(() => {
          this.init()
        }, 5000)
      } else {
        console.error(error)
      }
    }
  }
}

module.exports = App
