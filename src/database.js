// Mongoose
const mongoose = require('mongoose')

// JWT Wrapper
const { jwtSign } = require('./jwt')

// Models schemas
const UserModel = require('./models/user.model')
const HistoryModel = require('./models/history.model')

class Database {
  init() {
    const { DATABASE_HOST, DATABASE_PORT, DATABASE_NAME } = process.env

    return new Promise(async(resolve, reject) => {
      try {
        await mongoose.connect(`mongodb://${DATABASE_HOST || 'localhost'}:${DATABASE_PORT || 27017}/${DATABASE_NAME || 'db'}`, {useNewUrlParser: true})

        this.db = mongoose.connection

        this.db.on('error', (error) => {
          reject(error)
        })

        // Define models schemas
        this.user = mongoose.model('User', UserModel)
        this.history = mongoose.model('History', HistoryModel)

        resolve(true)
      } catch (error) {
        reject(error)
      }
    })
  }

  /**
   * Create a new user
   * 
   * @param {String} name 
   * @param {Strig} email 
   * @param {String} password 
   * @return {Promise<UserModel>}
   */
  createUser(name, email, password) {
    return new Promise((resolve, reject) => {
      this.user.findOne({
        email: email.toLowerCase()
      }, (err, doc) => {
        if (err) reject(err)
        if (doc) reject('EMAIL_EXISTS')

        this.user.create({
          name,
          email,
          password
        }).then(async (user) => {
          const token = await jwtSign(user.id, user.email)
          user.token = token
          user.save()

          resolve(user)
        }, err => reject(err))
      })
    })
  }

  /**
   * Create a new history to user
   * 
   * @param {String} user 
   * @param {String} email 
   * @param {Boolean} valid 
   */
  createHistory(user, email, valid) {
    return this.history.create({
      _creator: user.id,
      email,
      valid
    })
  }

  /**
   * Get user from database
   * 
   * @param {Object} conditions 
   */
  getUser(conditions) {
    return new Promise((resolve, reject) => {
      this.user.findOne(conditions, (err, doc) => {
        if (err) reject(err)
        else resolve(doc)
      })
    })
  }

  /**
   * Get user history
   * 
   * @param {String} userId 
   * @param {String} search 
   */
  getUserHistory(userId, search) {
    return new Promise((resolve, reject) => {
      let opts = {
        _creator: userId
      }

      if (search.length > 0) {
        opts['email'] = {
          $regex: `.*${search}.*`
        }
      }

      this.history.find(opts).sort({
        createdAt: 'desc'
      }).exec((err, doc) => {
        if (err) reject(err)
        else resolve(doc)
      })
    })
  }
}

module.exports = new Database()
