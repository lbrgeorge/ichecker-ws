// ExpressJS
const express = require('express')

// Cors
const cors = require('cors')

// Body Parser
const bodyParser = require('body-parser')

// Routing
const routesV1 = require('./routes/v1')

class Http {
  /**
   * Configure HTTP Server
   * 
   * @param {Number} port 
   * @return {Http}
   */
  configure(port = 8081) {
    this.port = port
    return this
  }
  
  /**
   * Initialize HTTP Server
   * 
   * @return {Promise<Number>}
   */
  init() {
    return new Promise((resolve, reject) => {
      if (this.port !== undefined) {
        // Initialize express app server
        this.app = express()
        this.app.listen(this.port, () => {
          // Setup our cors
          this.app.use(cors({
            exposedHeaders: ['Authorization']
          }))

          // Parse all requests body data to json
          this.app.use(bodyParser.json())
          // Parse encoded data url
          this.app.use(bodyParser.urlencoded({extended: true}))

          // Routing versioning
          this.app.use('/v1', routesV1)

          // Everything sounds fine!
          resolve(this.port)
        })
      } else {
        reject('Please configure the HTTP server with "configure" method!')
      }
    })
  }
}

module.exports = new Http()
