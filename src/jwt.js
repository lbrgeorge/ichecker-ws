const fs = require('fs')
const path = require('path')

// JWT
const jwt = require('jsonwebtoken')

// Keypairs
const keypair = require('keypair')

// RSA Keys
const privateKey = path.join(__dirname, "../keys/private.key")
const publicKey = path.join(__dirname, "../keys/public.key")

// Sign informations
const { JWT_COMPANY, JWT_EMAIL, JWT_WEBSITE, JWT_EXPIRE } = process.env
const signOptions = {
  issuer: JWT_COMPANY,
  subject: JWT_EMAIL,
  audience: JWT_WEBSITE,
  expiresIn: JWT_EXPIRE,
  algorithm: 'RS256'
}

/**
 * Validate signing public and private keys
 * 
 * @return {Boolean}
 */
const validateKeys = async () => {
  const existsPrivate = await fs.existsSync(privateKey)
  const existsPublic = await fs.existsSync(publicKey)

  // Does both keys exist?
  if (!existsPrivate || !existsPublic) {
    return false
  }

  // Test keys
  try {
    // Get keys
    const rawPrivateKey = await fs.readFileSync(privateKey, {encoding: 'UTF8'})
    const rawPublicKey = await fs.readFileSync(publicKey, {encoding: 'UTF8'})

    // Create fake token just for testing
    const token = await jwt.sign({test: new Date().getTime()}, rawPrivateKey, signOptions)
    // Test fake token
    await jwt.verify(token, rawPublicKey, signOptions)

    return true
  } catch (error) {
    console.error(error)
    return false
  }
}

/**
 * Generate new public and private RSA keys
 */
const generateKeys = async () => {
  // Create pair of keys
  const { public, private } = await keypair()

  // Check existing keys
  const existsPrivate = await fs.existsSync(privateKey)
  const existsPublic = await fs.existsSync(publicKey)

  // Delete existing keys
  if (existsPrivate) {
    await fs.unlinkSync(privateKey)
  }
  if (existsPublic) {
    await fs.unlinkSync(publicKey)
  }

  // Create new keys
  await fs.writeFileSync(privateKey, private, {encoding: 'UTF8'})
  await fs.writeFileSync(publicKey, public, {encoding: 'UTF8'})
}

/**
 * Sign a new JWT token
 * 
 * @param {String} userId 
 * @param {String} email 
 * @return {Promise<String>}
 */
const jwtSign = (userId, email) => {
  return new Promise((resolve, reject) => {
    fs.readFile(privateKey, {encoding: 'UTF8'}, (err, raw) => {
      if (err) reject(err)
      else {
        try {
          const token = jwt.sign({
            date: new Date().getTime(),
            userId,
            email
          }, raw, signOptions)

          resolve(token)
        } catch (error) {
          reject(error)
        }
      }
    })
  })
}

/**
 * Verify a JWT token
 * 
 * @param {String} token 
 * @return {Promise}
 */
const jwtVerify = (token) => {
  return new Promise((resolve, reject) => {
    fs.readFile(publicKey, {encoding: 'UTF8'}, (err, raw) => {
      if (err) reject(err)
      else {
        try {
          const legit = jwt.verify(token, raw, signOptions)
          resolve(legit)
        } catch (error) {
          reject(error)
        }
      }
    })
  })
}

module.exports = {
  validateKeys,
  generateKeys,
  jwtSign,
  jwtVerify
}
