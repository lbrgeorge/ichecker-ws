// JWT
const { jwtVerify } = require('../jwt')

// Database
const database = require('../database')

module.exports = (req, res, next) => {
  var auth = req.headers.authorization

  if (!auth) {
    res.status(403).json({ error: 'Missing authorization' })
  } else {
    const token = auth.replace('Bearer ', '')
    jwtVerify(token).then(result => {
      const { email } = result

      if (email !== undefined) {
        database.getUser({
          email,
          token
        }).then(user => {
          if (user === null) res.status(403).json({error: 'INVALID_TOKEN'})
          else {
            req.user = user
            next()
          }
        })
      } else {
        res.status(403).json({error: 'INVALID_TOKEN'})
      }
    }, error => {
      res.status(403).json({ error })
    })
  }
}
