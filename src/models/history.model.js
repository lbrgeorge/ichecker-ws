// Mongoose schema struct
const { Schema } = require('mongoose')

module.exports = new Schema({
  _creator: {type: Schema.Types.ObjectId, ref: 'User'},
  email: String,
  valid: Boolean,
  createdAt: {
    type: Date,
    default: Date.now
  }
})
