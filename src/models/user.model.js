// Mongoose schema struct
const { Schema } = require('mongoose')

module.exports = new Schema({
  name: String,
  email: String,
  password: String,
  token: String,
  history: [{type: Schema.Types.ObjectId, ref: 'History'}],
  createdAt: {
    type: Date,
    default: Date.now
  },
  updatedAt: {
    type: Date,
    default: Date.now
  }
})
