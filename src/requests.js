module.exports = [
  /**
   * Register route
   */
  {
    request: '/v1/register',
    body: [
      {
        name: 'name',
        required: true
      },
      {
        name: 'email',
        required: true
      },
      {
        name: 'password',
        required: true
      }
    ]
  },
  /**
   * Login route
   */
  {
    request: '/v1/login',
    body: [
      {
        name: 'email',
        required: true
      },
      {
        name: 'password',
        required: true
      }
    ]
  },
  /**
   * Email validator route
   */
  {
    request: '/v1/email/validate',
    body: [
      {
        name: 'email',
        required: true
      }
    ]
  }
]
