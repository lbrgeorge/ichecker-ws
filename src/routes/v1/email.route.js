// Express Router
const router = require('express').Router()

// Database
const database = require('../../database')

// Email validator
const emailValidator = require('../../email-validator')

// Requests rules
const requestRules = require('../../middleware/rules.middleware')
router.use(requestRules)

// Authentication only
const auth = require('../../middleware/auth.middleware')
router.use(auth)

router.post('/validate', (req, res) => {
  const { email, identifier } = req.body

  const isValid = emailValidator(email)
  database.createHistory(req.user, email, isValid).then(history => {
    const { _id, email, valid, createdAt } = history
    let validation = {
      _id,
      email,
      valid,
      createdAt
    }

    if (identifier) {
      validation.identifier = identifier
    }

    res.json({
      validation
    })
  }, error => {
    res.status(403).json(error)
  })
})

router.get('/history', (req, res) => {
  const { search } = req.query
  const { id } = req.user

  database.getUserHistory(id, search || '').then(history => {
    res.json({history})
  }, error => {
    res.status(403).json(error)
  })
})

module.exports = router
