// Express Router
const router = require('express').Router()

// Routes
const registerRoute = require('./register.route')
const loginRoute = require('./login.route')
const emailRoute = require('./email.route')

// Routing
router.use('/register', registerRoute)
router.use('/login', loginRoute)
router.use('/email', emailRoute)

module.exports = router
