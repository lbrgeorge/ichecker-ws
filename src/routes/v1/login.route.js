// Express Router
const router = require('express').Router()

// Database
const database = require('../../database')

// SHA512
const { encryptSHA512 } = require('../../sha512')

// JWT sign
const { jwtSign } = require('../../jwt')

// Requests rules
const requestRules = require('../../middleware/rules.middleware')
router.use(requestRules)

/**
 * Log in user
 */
router.post('/', (req, res) => {
  const { email, password } = req.body

  database.getUser({
    email: email,
    password: encryptSHA512(password)
  }).then(async (user) => {
    if (user) {
      const token = await jwtSign(user.id, user.email)
      user.token = token
      user.save()

      res.json({user})
    } else {
      res.status(404).json({error: 'USER_NOT_FOUND'})
    }
  }, error => res.status(403).json({error}))
})

module.exports = router
