// Express Router
const router = require('express').Router()

// Database
const database = require('../../database')

// SHA512
const { encryptSHA512 } = require('../../sha512')

// Email validator
const emailValidator = require('../../email-validator')

// Requests rules
const requestRules = require('../../middleware/rules.middleware')
router.use(requestRules)

/**
 * Register a new user
 */
router.put('/', async (req, res) => {
  const { name, email, password } = req.body

  const isEmailValid = emailValidator(email)
  if (!isEmailValid) res.status(403).json({error: 'INVALID_EMAIL'})
  else {
    database.createUser(name, email, encryptSHA512(password)).then(user => {
      res.json({user})
    }, error => {
      res.status(403).json({error})
    })
  }
})

module.exports = router
