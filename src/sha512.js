// Crypto
const crypto = require("crypto")

/**
 * Encrypt text to SHA512
 * 
 * @param {String} text 
 * @return {String}
 */
const encryptSHA512 = (text) => {
  // Create hash
  const hash = crypto.createHash('sha512')
  
  // Hashing text
  const hashed = hash.update(text, 'utf8')

  // Pass hashed text to HEX, just for dificulty
  return hashed.digest('hex')
}

module.exports = {
  encryptSHA512
}
